﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementHandler : MonoBehaviour {

    public Animator BodyAnimator;
    public List<float> MovementAnimationSelections;
    public float TransitionSpeed;
    public float Counter;
    public float MaxTime;
    public float AnimationState;
    public float TargetState;
    public bool IsHittingHead = false;

	void Start () {
        BodyAnimator = GetComponent<Animator>();
        SetAnimationThresholds();
        ResetCounter();
        SetMovement();
	}
	
	void Update () {
        Counter -= Time.deltaTime;
        if ((Counter <= 0) && (IsHittingHead == false))
        {
            SetMovement();
            ResetCounter();
        }
        else
        {
            UpdateMovement();
            UpdateCounter();
        }
	}

    private void SetAnimationThresholds()
    {
        MovementAnimationSelections.Add(0.12f);
        MovementAnimationSelections.Add(0.42f);
        MovementAnimationSelections.Add(0.71f);
        MovementAnimationSelections.Add(0.1f);
    }

    private void ResetCounter(float value = 0f)
    {
        if (value == 0f)
        {
            Counter = MaxTime;
        }
        else
        {
            Counter = value;
        }
    }

    private void UpdateCounter()
    {
        Counter -= Time.deltaTime;
        if (Counter <= 0)
        {
            BodyAnimator.SetBool("IsHittingHead", false);
            IsHittingHead = false;
            Counter = 0;
        }
    }

    public void HitHead()
    {
        if (IsHittingHead == false)
        {
            BodyAnimator.SetBool("IsHittingHead", true);
            ResetCounter(3f);
        }
    }

    private void SetMovement()
    {
        var animation_index = Random.Range(0, MovementAnimationSelections.Count);
        TargetState = MovementAnimationSelections[animation_index];
        BodyAnimator.SetBool("IsHittingHead", false);
    }

    private void SetMovement(float state)
    {
        TargetState = state;
    }

    private void UpdateMovement()
    {
        AnimationState = BodyAnimator.GetFloat("Blend");
        IsHittingHead = BodyAnimator.GetBool("IsHittingHead");

        if (AnimationState < TargetState - 0.02)
        {
            if (AnimationState < 1)
            {
                BodyAnimator.SetFloat("Blend", AnimationState + 0.3f * Time.deltaTime * TransitionSpeed);
            }
        } 
        else if (AnimationState > TargetState + 0.02)
        {
            if (AnimationState > 0)
            {
                BodyAnimator.SetFloat("Blend", AnimationState - 0.3f * Time.deltaTime * TransitionSpeed);
            }
        }
    }
}
