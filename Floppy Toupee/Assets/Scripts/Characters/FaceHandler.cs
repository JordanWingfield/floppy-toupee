﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceHandler : MonoBehaviour {

    public float TimeCounter;
    public float MinTime;
    public float MaxTime;
    public List<Sprite> Faces;

	void Start () {
        ResetCounter();
	}

    void Update() {
        Countdown();
        if (TimeCounter <= 0)
        {
            ChangeFace();
            ResetCounter();
        }
    }

    public void GetHit()
    {
        ResetCounter();
        GetComponent<SpriteRenderer>().sprite = Faces[3];
    }

    private void ChangeFace()
    {
        var sprite_index = Random.Range(0, Faces.Count);
        if (sprite_index < Faces.Count)
        {
            GetComponent<SpriteRenderer>().sprite = Faces[sprite_index];
        }
    }

    private void Countdown()
    {
        TimeCounter -= 1;
    }

    private void ResetCounter()
    {
        TimeCounter = Random.Range(MinTime, MaxTime);
    }
}
