﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HairHandler : MonoBehaviour {

    public Rigidbody2D RigidBody;
    public float Velocity = 1f;

	// Use this for initialization
	void Start () {
        RigidBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
        if (IsDead())
        {
            Destroy(this.gameObject);
        }

        foreach (var touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                RigidBody.AddRelativeForce(new Vector2(0, Velocity), ForceMode2D.Impulse);
            }
        }
	}

    private bool IsDead()
    {
        if (transform.position.y <= 10)
        {
            return true;
        }
        return false;
    }
}
