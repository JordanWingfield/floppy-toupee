﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandHandler : MonoBehaviour {

    public float Velocity;
    public float ForceAmount;

    public float TimeCounter;
    public float MinTime;
    public float MaxTime;
    public List<Sprite> Hands;

    private void Start()
    {
        ResetCounter();
    }

    private void Update()
    {
        Countdown();
        if (TimeCounter <= 0)
        {
            ResetCounter();
            ChangeHand();
        }
    }

    private void Countdown()
    {
        TimeCounter -= Time.deltaTime;
        if (TimeCounter <= 0)
        {
            TimeCounter = 0;
        }
    }

    private void ResetCounter()
    {
        TimeCounter = Random.Range(MinTime, MaxTime);
    }

    private void ChangeHand()
    {
        var sprite_index = Random.Range(0, Hands.Count);
        GetComponent<SpriteRenderer>().sprite = Hands[sprite_index];
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Toupee")
        {
            ApplyForce(collision.gameObject.GetComponent<Rigidbody2D>());
        }
    }

    private void ApplyForce(Rigidbody2D body)
    {
        body.AddForce(new Vector2(-1, 1) * ForceAmount, ForceMode2D.Impulse);
    }
}
