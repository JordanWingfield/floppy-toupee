﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyHandler : MonoBehaviour {
    
    public Vector2 Target;
    public float Counter;

    private bool HitTarget = false;

    // Use this for initialization
    void Start () {
        GetTarget();
        SetupCounter();
	}
	
	// Update is called once per frame
	void Update () {
        if (!HitTarget) 
        {
            MoveRandomly();
        }
        else if (Counter <= 0)
        {
            DetachSelf();
            MoveRandomly();
        }
        else
        {
            UpdateCounter();
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (HitTarget == false)
        {
            if (collision.gameObject.tag == "Toupee")
            {
                HitTarget = true;
                transform.parent = collision.transform;
                GameObject.FindGameObjectWithTag("Body").GetComponent<MovementHandler>().HitHead();
                SetExitTarget();
                SetupCounter(5f);
            }
        }
    }

    private void SetupCounter(float new_count = 0)
    {
        Counter = new_count;
    }

    private void UpdateCounter()
    {
        Counter -= Time.deltaTime;
    }

    private void DetachSelf()
    {
        transform.parent = null;
    }

    private void SetExitTarget()
    {
        var x_positions = new float[] { -6, 6 };
        var y_positions = new float[] { -6, 6 };
        var x_pos = Random.Range(0, 2);
        var y_pos = Random.Range(0, 2);

        Target = new Vector2(x_positions[x_pos], y_positions[y_pos]);
        Destroy(this.gameObject, 100f);
    }

    private void MoveRandomly()
    {
        var x_min = Target.x - 10;
        var x_max = Target.x + 10;
        var y_min = Target.y - 10;
        var y_max = Target.y + 10;

        var current_pos = new Vector2(transform.position.x, transform.position.y);
        var destination_pos = new Vector2(Random.Range(x_min, x_max), Random.Range(y_min, y_max));
        transform.position = Vector2.MoveTowards(current_pos, destination_pos, 5f * Time.deltaTime);
    }
    
    private void GetTarget()
    {
        Target = GameObject.FindGameObjectWithTag("Head").transform.position;
    }
}
