﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFOHandler : MonoBehaviour {

    public GameObject TractorBeam;
    public Vector2 Target;
    public Vector2 HoverPosition;
    public float Counter = 0f;
    public float MaxCounter = 20f;
    public bool IsDone = false;
    public bool IsHovering = false;
    public bool IsBeamDeployed = false;
    public bool IsBeamRetracted = false;

	// Use this for initialization
	void Start () {
        GetTarget();
        SetPositions();
        MoveToHover();
        Counter = MaxCounter;
	}
	
	// Update is called once per frame
	void Update () {
        if (!IsHovering)
        {
            MoveToHover();
        }
        else if (!IsBeamDeployed)
        {
            CreateTractorBeam();
        }
        else if (!IsDone)
        {
            Wait();
        }
        else if (!IsBeamRetracted)
        {
            Destroy(TractorBeam);
            IsBeamRetracted = true;
            Counter = MaxCounter;
            IsDone = false;
        }
        else
        {
            MoveToEnd();
        }
	}

    private void MoveToHover()
    {
        var current_pos = new Vector2(transform.position.x, transform.position.y);
        transform.position = Vector2.MoveTowards(current_pos, HoverPosition, 3f * Time.deltaTime);
        if (current_pos == HoverPosition)
        {
            IsHovering = true;
        }
    }

    private void MoveToEnd()
    {
        var current_pos = new Vector2(transform.position.x, transform.position.y);
        var end_pos = new Vector2(Target.x, Target.y + 8);
        transform.position = Vector2.MoveTowards(current_pos, end_pos, 3f * Time.deltaTime);
        
        if (current_pos == end_pos)
        {
            Destroy(this.gameObject);
        }
    }

    private void CreateTractorBeam()
    {
        var beam_pos = new Vector3(this.transform.position.x, this.transform.position.y - 2, 0);
        TractorBeam = Instantiate(TractorBeam, beam_pos, new Quaternion(0, 0, 0, 0));
        IsBeamDeployed = true;
    }

    private void Wait()
    {
        Counter -= Time.deltaTime;
        if (Counter <= 0)
        {
            IsDone = true;
        }
    }

    private void SetPositions()
    {
        var StartPosition = new Vector2(Target.x, Target.y + 8);
        HoverPosition = new Vector2(Target.x, Target.y + 3);
        transform.Translate(StartPosition, Space.World);
    }

    private void GetTarget()
    {
        Target = GameObject.FindGameObjectWithTag("Toupee").transform.position;
    }
}
