﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TractorBeamHandler : MonoBehaviour {

    public float ForceAmount;
    public float Counter;
    public float MaxCount = 10f;
    public bool CanPull = true;

    private void Start()
    {
        Counter = MaxCount;
    }

    private void Update()
    {
        Counter -= Time.deltaTime;
        if (Counter <= 0)
        {
            CanPull = true;
            Counter = MaxCount;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (CanPull == true)
        {
            ApplyForce(collision.gameObject.GetComponent<Rigidbody2D>());
            CanPull = false;
        }
    }

    private void ApplyForce(Rigidbody2D body)
    {
        body.AddForce(new Vector2(-2, 3) * ForceAmount, ForceMode2D.Impulse);
    }
}
