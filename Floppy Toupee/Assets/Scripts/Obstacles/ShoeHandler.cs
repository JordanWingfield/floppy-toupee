﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShoeHandler : MonoBehaviour {

    public float Velocity;
    public float ForceAmount;
    public Vector2 Target;
    public float AngleThreshold;

    private bool HitTarget = false;
    
	void Start () {
        
        if(GetTarget())
        {
            Move();
        }
        else
        {
            Target = new Vector2(0, 3);
            HitTarget = true;
        }
	}
	
	void Update () {
        if (!HitTarget && Target != null)
        {
            Move();
        }
        else if (IsOffScreen())
        {
            Destroy(this.gameObject);
        }
	}

    private bool GetTarget()
    {
        if (GameObject.FindGameObjectWithTag("Toupee") != null)
        {
            Target = GameObject.FindGameObjectWithTag("Toupee").transform.position;
            return true;
        }
        return false;
    }

    private void Move()
    {
        transform.position = Vector2.MoveTowards(transform.position, Target, 20f * Time.deltaTime);
        transform.Rotate(new Vector3(0, 0, 1), 1000 * Time.deltaTime);
    }

    private bool IsOffScreen()
    {
        if (transform.position.y <= -10)
        {
            return true;
        }
        return false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Toupee")
        {
            HitTarget = true;
            ApplyForce(collision.gameObject.GetComponent<Rigidbody2D>());
            transform.rotation = new Quaternion(-1, 0, 0, 0);
            transform.Translate(new Vector3(0, -10, 0) * Velocity * Time.deltaTime, Space.World);
        }
    }

    private void ApplyForce(Rigidbody2D body)
    {
        body.AddForce(new Vector2(1, 1) * ForceAmount, ForceMode2D.Impulse);
    }
}
