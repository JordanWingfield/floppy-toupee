﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GravityUpdate : MonoBehaviour {

    public GameObject HairPiece;
    public GameObject Head;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        var text = GetComponent<Text>();
        
        if (HairPiece != null)
        {
            if (HairPiece.GetComponent<AngleHandler>().IsFallen())
            {
                text.text = "Game Over";
            }
            else
            {
                text.text = "Angle: " + Head.GetComponent<RotationHandler>().Angle;
                text.text += ", Hair Angle: " + HairPiece.GetComponent<AngleHandler>().Angle;
            }  
        }
        
	}
}
