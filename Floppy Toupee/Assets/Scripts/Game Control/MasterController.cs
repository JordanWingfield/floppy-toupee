﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// ===================================================================
// Characters are listed in indexed order for organization, and can
// be accessed as follows:
// 0: Trump
// 1: Pence
// 2: Trudeau
// 3: Clinton
// 4: Putin
// 5: Toupee
// ===================================================================

// ===================================================================
// Obstacles are listed in indexed order for organization, and can be
// accessed as follows:
// 0: Shoe
// 1: Eagle
// 2: Fly
// 3: UFO
// ===================================================================

public class MasterController : MonoBehaviour {

    public GameObject Gamedata;

    public List<GameObject> Characters;
    public List<Transform> ChildrenTransforms;
    public GameObject Podium;
    public GameObject Toupee;

    public List<GameObject> Obstacles;
    public float TimeCounter;
    public float MinTime;
    public float MaxTime;

    public Transform canvas;
    public GameObject gameOverScreenPrefab;
    private GameObject gameOverPanel;
    public GameObject leaderBoardPanelPrefab;
    public GameObject leaderBoardPanel;
    public GameObject networkErrorMessagePrefab;

    private bool gameOver = false;
    private bool hasSubmittedScore = false;
    private bool hasCreatedLeaderboard = false;

    public int Score;
    public string Name;
    public int ScoreIncrement;
    public Text podiumBannerText;
    
	public enum GameState {Game_Start, Game_Play, Game_Over, Game_Scores,Game_Wait};
    public GameState State;

	void Start () {
        State = GameState.Game_Start;
        if (GetGamedata() == true)
        {
            SetupAssets();
            SetupObstacles();
            SetupScoreboard();
            State = GameState.Game_Play;
        }
        else
        {
            Debug.Log("Error: No object with tag 'Gamedata' found.");
        }
	}

	void Update () {
		if (State == GameState.Game_Play)
        {
            CheckHairState();
            GenerateObstacles();
            UpdateScore();
        }
        else if (State == GameState.Game_Over)
        {
            DisplayGameOver();
        }
        else if (State == GameState.Game_Scores)
        {
            DisplayLeaderBoard();
        }
        else if (State == GameState.Game_Start)
        {
            // Debug.Log("Error: Game has yet to start.");
        }
	}

    private void CheckHairState()
    {

        if (Toupee.GetComponent<AngleHandler>().HasFallen == true)
        {
            State = GameState.Game_Over;
            return;
        }
    }

    private bool GetGamedata()
    {
        Gamedata = GameObject.FindWithTag("Gamedata");
        if (Gamedata != null)
        {
            return true;
        }
        return false;
    }

    private void SetupAssets()
    {
        // TODO: replace default value with actual integer from the Gamedata object.
        var character_index = Gamedata.GetComponent<GamedataHandler>().CharacterIndex;

        Instantiate(Characters[character_index], ChildrenTransforms[0]);
        var podium = Instantiate(Podium, ChildrenTransforms[1]);
        podiumBannerText = podium.GetComponentInChildren<Text>();
        Toupee = GameObject.FindGameObjectWithTag("Toupee");
    }

    private void SetupObstacles()
    {
        ResetCounter();
    }

    private void SetupScoreboard()
    {
        Score = 0;
        // TODO: update score on the banner setup to 
        // display.
        DisplayScore();
    }

    private void GenerateObstacles()
    {
        Countdown();
        if (TimeCounter <= 0)
        {
            var obstacle_index = Random.Range(0, 4);
            switch (obstacle_index)
            {
                case 0:
                    CreateShoe();
                    break;
                case 1:
                    CreateEagle();
                    break;
                case 2:
                    CreateFly();
                    break;
                case 3:
                    CreateUFO();
                    break;
                default:
                    Debug.Log("Error: obstacle_index out of bounds for range of objects.");
                    break;
            }
            ResetCounter();
        }
    }

    private void CreateShoe()
    {
        var x = Random.Range(-5, 5);
        Instantiate(Obstacles[0], new Vector3(x, -6, -10), new Quaternion(0, 0, 0, 0));
    }

    private void CreateEagle()
    {
        Instantiate(Obstacles[1], new Vector3(-5, 0, -10), new Quaternion(0, 0, 0, 0));
    }

    private void CreateFly()
    {
        var x = Random.Range(-5, 5);
        var y = Random.Range(-7, 7);
        Instantiate(Obstacles[2], new Vector3(x, y, -10), new Quaternion(0, 0, 0, 0));
    }

    private void CreateUFO()
    {
        var xs = new List<int>();
        xs.Add(-5);
        xs.Add(5);
        var x = Random.Range(0, 1);
        Instantiate(Obstacles[3], new Vector3(x, 0, -10), new Quaternion(0, 0, 0, 0));
    }

    private void Countdown()
    {
        TimeCounter -= Time.deltaTime;
        if (TimeCounter <= 0)
        {
            TimeCounter = 0;
        }
    }

    private void ResetCounter()
    {
        TimeCounter = Random.Range(MinTime, MaxTime);
    }

    private void UpdateScore()
    {
        Score += ScoreIncrement;
        DisplayScore();
    }

    private void DisplayScore()
    {
        podiumBannerText.text = Score.ToString();
    }

    private void DisplayGameOver()
    {
        if (gameOver == false)
        {
            Debug.Log("Displaying GameOver");
            gameOverPanel = Instantiate(gameOverScreenPrefab, canvas);
            gameOverPanel.GetComponent<GameOverManager>().gameScore = (int)Score;
            gameOverPanel.GetComponent<GameOverManager>().scoreText.text = Score.ToString();
            gameOver = true;
            Gamedata.GetComponent<GamedataHandler>().Save(Score);
        }
        else
        {
            CheckForSubmit();
        }
        
    }

    private void DisplayLeaderBoard()
    {
        if (!hasCreatedLeaderboard)
        {
            StartCoroutine(ToggleLeaderBoardAnimation());
            hasCreatedLeaderboard = true;
        }
    }

    private void CheckForSubmit()
    {
        var input_name = gameOverPanel.GetComponent<GameOverManager>().GetSubmit();
        if (input_name != null)
        {
            var isNoError = SendScoreToLeaderBoard(input_name);
            if (isNoError)
            {
                Name = input_name;
                State = GameState.Game_Scores;
                hasSubmittedScore = true;
            }
        }
    }

    private bool SendScoreToLeaderBoard(string name)
    {
        // This is the name the player inputs:
        //gameOverPanel.GetComponent<GameOverManager>().inputName.text
        // You may want to do a null check so that it doesn't send to the server if it's empty.
        // TODO: Check name == null
        // TODO: Add network connection check here.
        //var info = gameObject.GetComponent<HSController>().PostScore(name, Score);

        if (name != "")
        {
            var info = gameObject.GetComponent<HSController>();
            info.PostScore(name, Score);

            if (info.GetIsError())
            {
                // Create Error message here.
                var otherMessage = GameObject.FindGameObjectWithTag("ErrorMessage");

                if (otherMessage == null)
                {
                    var errorMessage = Instantiate(networkErrorMessagePrefab, canvas);
                    return false;
                }
                else
                {
                    Debug.Log("No duplicate gameobject with tag: 'ErrorMessage', found.");
                }
            }
            else
            {
                Debug.Log("Sending name=" + name + ", score=" + Score.ToString() + " to server.");
            }
        }
        else
        {
            // Name is empty, allow button to reset, but do not progress game state.
            return false;
        }
        return true;
    }

    IEnumerator ToggleLeaderBoardAnimation()
    {
        gameOverPanel.GetComponent<GameOverManager>().SwitchLeaderBoardPanel();
        leaderBoardPanel = Instantiate(leaderBoardPanelPrefab, canvas);
        yield return new WaitForSeconds(0.2f);
        var isNoError = LoadHighScoresIntoLeaderBoard();
        if (isNoError)
        {
            leaderBoardPanel.GetComponent<Animator>().Play("LeaderBoardEnter_Anim");
        }
    }

    private bool LoadHighScoresIntoLeaderBoard()
    {
        // TODO: Add network connection check here.
        // var info = gameObject.GetComponent<HSController>().GetScore();

        var info = gameObject.GetComponent<HSController>();
        var info_score = info.GetScore();

        Debug.Log("DB Query: " + info_score);

        if (info.GetIsError())
        {
            // Create Error message here. Do not try to parse.
            var otherMessage = GameObject.FindGameObjectWithTag("ErrorMessage");

            if (otherMessage == null)
            {
                var errorMessage = Instantiate(networkErrorMessagePrefab, canvas);
            }
            else
            {
                Debug.Log("No duplicate gameobject with tag: 'ErrorMessage', found.");
            }
            return false;
        }
        else
        {

            Debug.Log("Loading high scores ...");

            string[] splitOptions = { "\r\n", "\n", "\t" };
            var scoresList = info_score.Split(splitOptions, 15, System.StringSplitOptions.None);

            // You can access each entry of the leaderboard by calling the relevant arrays in the leaderBoardPanel after it's instantiated.
            int i = 0;
            int j = 0;
            
            while (i < scoresList.Length && j < 5 && scoresList.Length != 1)
            {
                leaderBoardPanel.GetComponent<LeaderBoardManager>().names[j].text = scoresList[i];
                leaderBoardPanel.GetComponent<LeaderBoardManager>().scores[j].text = scoresList[i + 1];
                j += 1;
                i += 2;
            }

            leaderBoardPanel.GetComponent<LeaderBoardManager>().names[5].text = Name;
            leaderBoardPanel.GetComponent<LeaderBoardManager>().scores[5].text = Score.ToString();
            // for the last position, enter the score just submitted, and name submitted.
        }

        return true;
    }
    
    public void LoadHighScores()
    {
        LoadHighScoresIntoLeaderBoard();
    }

}
