﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Character Unlock Thresholds Class
 * 
 * Written by: Jordan R. Wingfield
 * Date: 09-04-17
 * 
 * Contains the thresholds for character unlocking basded off of cumulative score
 * capturing at the end of a game session.
 */

public class UnlockThresholds : MonoBehaviour {
    
    public void NextTier()
    {
        Debug.Log("Setting up next tier.");
        CurrentTier += 1;
        if (CurrentTier > m_MaxTier)
        {
            CurrentTier = m_MaxTier;
        }
    }

    public int GetTier(float score)
    {
        for (int i = 1; i < m_MaxTier; i++)
        {
            float currentThreshold = Mathf.Pow(2, i) * 1000;
            if (score <= currentThreshold)
            {
                // CurrentTier = i;
                return i;
            }
        }
        return m_MaxTier;
    }

    public float GetCurrentThreshold(int tier)
    {
        if (tier <= 0)
        {
            return 0;
        }
        return (Mathf.Pow(2, tier) * 1000);
    }

    public int CurrentTier = 1;
    private int m_MaxTier = 6;
    
}
