﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* Character Unlock Handler Class
 * 
 * Written By: Jordan R. Wingfield
 * Date: 09-15-17
 * 
 * This class handles the setup and termination of unlock notification
 * UI objects. 
 * 
 */

public class UnlockCharHandler : MonoBehaviour {

    public Text CharacterName;
    public Image CharacterImage;

    public string[] CharacterNames;
    public Sprite[] CharacterImages;

    GameOverManager GameoverManager;
    
    // Sets the head and name for newly unlocked character
    public void Setup(int index, GameOverManager gom)
    {
        if (index >= 0 && index < CharacterNames.Length)
        {
            CharacterName.text = CharacterNames[index];
            CharacterImage.sprite = CharacterImages[index];
        }
        else
        {
            Debug.Log("Error: UnlockCharHandler - Index requested outside bounds of names and images array. Index requested: " + index + ", Max Size: " + CharacterNames.Length + ".");
        }
        GameoverManager = gom;
    }

    // Destroy window, and resume counting for score gained.
    public void Close()
    {
        GameoverManager.IsUpdating = true;
        DestroyObject(this.gameObject);
    }
}
