﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Character Selection Hanlder Class
 * 
 * Written by: Jordan R. Wingfield
 * Date: 09-18-17
 * 
 * Manages which characters can be selected via saved game data.
 */ 

public class CharacterSelectHandler : MonoBehaviour {
    
    // Called on initialization. 
	void Start () {
        GetSaveData();
        SetCharactersToPlay();
	}

    // Fetches Gamedata game object, if there is one.
    private void GetSaveData()
    {
        var go = GameObject.FindGameObjectWithTag("Gamedata");
        if (go != null)
        {
            Gamedata = go.GetComponent<GamedataHandler>();
            Gamedata.Load();
        }
    }

    // If there is an available Gamedata object, will make character selections
    // available based on tier aquired. 
    private void SetCharactersToPlay()
    {
        var max_tier = Gamedata.Data.HighestTier;

        for (int i = 1; i <= 5; i++)
        {
            if (max_tier > i || max_tier > Characters.Length)
            {
                Characters[i - 1].SetActive(true);
            }
        }
    }

    public GamedataHandler Gamedata;
    public GameObject[] Characters;
}
