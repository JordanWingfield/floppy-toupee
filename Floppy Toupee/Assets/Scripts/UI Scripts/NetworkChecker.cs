﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class NetworkChecker : MonoBehaviour {

    public bool isConnected = false;
    public string url = "https://www.google.ca/?gfe_rd=cr&ei=d72LWdXpC6zj8AepuZCABQ";
    public Button Button;
    public Image Image;
    private float counter;
    private float max_counter = 2f;

    void Start()
    {
        Button = GetComponent<Button>();
        Image = GetComponent<Image>();
        counter = max_counter;

        StartCoroutine(CheckConnection());
    }

    void Update()
    {
        counter -= Time.deltaTime;
        if (counter <= 0f)
        {
            counter = max_counter;
            StartCoroutine(CheckConnection());
        }

        if (isConnected == true)
        {
            // Enable button and change sprite colour to normal
            Image.color = new Color(255, 255, 255, 255);
            Button.interactable = true;
        }
        else
        {
        }
    }

    IEnumerator CheckConnection()
    {
        Debug.Log("Checking connection ... ");
        var www = new WWW(url);
        yield return www;
        
        if (www.isDone == true && www.text != "")
        {
            isConnected = true;
        }
        else
        {
            isConnected = false;
            Debug.Log("Network error:" + www.error);
        }
    }
}
