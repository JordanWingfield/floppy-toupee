﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HowToButton : MonoBehaviour {

    public void LoadScene()
    {
        StartCoroutine(GoToGameScene(0.1f));
    }

    IEnumerator GoToGameScene(float time)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadSceneAsync(3);
    }
}
