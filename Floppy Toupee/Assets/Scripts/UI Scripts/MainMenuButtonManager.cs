﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuButtonManager : MonoBehaviour {

    public Animator leaderBoardAnimator;
    private bool leaderBoardState = false;
    public GameObject GameMaster;

    void Start()
    {
        GameMaster = GameObject.FindWithTag("Gamemaster");
    }

    public void StartCandidateScene()
    {
        SceneManager.LoadSceneAsync(1);
    }

    public void LoadLeaderBoards()
    {
        if (GameMaster != null)
        {
            var network_check = GameMaster.GetComponent<HSController>();
            network_check.GetScore();
            if (leaderBoardState == false && network_check.GetIsError() == false)
            {
                Debug.Log("Showing Leader Board");
                ShowLeaderBoards();
            }
            else
            {
                Debug.Log("Hiding Leader Board");
                HideLeaderBoards();
            }
        }
        else
        {
            Debug.Log("Error: No Object with tag Gamemaster found.");
        }
    }

    void ShowLeaderBoards()
    {
        if (leaderBoardState == false)
        {
            leaderBoardState = true;
            leaderBoardAnimator.Play("LeaderBoardEnter_Anim");
        }
    }

    void HideLeaderBoards()
    {
        if (leaderBoardState == true)
        {
            leaderBoardState = false;
            leaderBoardAnimator.Play("LeaderBoardExit_Anim");
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }


}
