﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkErrorManager : MonoBehaviour {

    public Animator animator;

	public void Close()
    {
        StartCoroutine(CloseSelf());
    }

    IEnumerator CloseSelf()
    {
        Debug.Log("Closing ... ");
        animator.Play("ErrorMessage-Exit_Anim");
        yield return new WaitForSeconds(0.5f);
        DestroyObject(this.gameObject);
    }
}
