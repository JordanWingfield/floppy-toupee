﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class CandidateButtonManager : MonoBehaviour {

    public GameObject gameData;

    [Space(20)]
    public int candidateIndexNumber;
    public GameObject candidatePanelCover;
    public GameObject checkmarkIcon;
    public Animator candidatePanelAnimator;

    private void Start()
    {
        if (gameData == null)
        {
            Debug.LogError("GameData object not found. Searching for it.");
            
            gameData = GameObject.Find("Gamedata");
        }
    }

    public void SelectCandidate()
    {
        checkmarkIcon.SetActive(true);
        candidatePanelCover.SetActive(true);

        // Play checkmark sound effect

        if (gameData != null)
        {
            gameData.GetComponent<GamedataHandler>().CharacterIndex = candidateIndexNumber;
        }

        candidatePanelAnimator.Play("SelectCandidate_Anim");
        StartCoroutine(GoToGameScene(1.5f));
    }

    IEnumerator GoToGameScene(float time)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadSceneAsync(2);
    }
}
