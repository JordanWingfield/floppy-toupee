﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour {

    public GameObject gameData;
    public GameObject gameMaster;

    public GameObject CharacterUnlockPrefab;
    public Transform Canvas;

    public UnlockThresholds ScoreThresholds;
    public int gameScore;
    public Text scoreText;
    public RectTransform totalScoreBarFill;
    public int totalScore;
    public int finalTotalScore;
    public Text totalScoreText;
    public RectTransform totalScoreBarFillMax;
    public float RectHeight;
    public float RectMaxWidth;
    public bool IsUpdating = false;
    private int scoreIncrement = 1;
    private float accelerationRate = 1;

    private int previousHighScore;

    public Text inputName;

    public bool isWaiting;


	private void Start () {
        gameData = GameObject.Find("Gamedata");
        gameMaster = GameObject.Find("Gamemaster");
        ScoreThresholds = GetComponent<UnlockThresholds>();
        Canvas = GameObject.FindGameObjectWithTag("MaingameCanvas").transform;

        isWaiting = true;

        // Load previous score for comparison.
        SetupTotalScore();
        StartCoroutine(StartUpdating());
	}

    IEnumerator StartUpdating()
    {
        yield return new WaitForSeconds(2.0f);
        IsUpdating = true;
    }

    private void Update()
    {
        if (IsUpdating)
        {
            UpdateTotalScore();
            IncrementTotalScore();
        }
    }

    // Set the total score bar fill and text to the current total score from gamedata
    private void SetupTotalScore()
    {
        finalTotalScore = gameData.GetComponent<GamedataHandler>().Data.TotalScore;
        totalScore = finalTotalScore - gameScore;
        UpdateTotalScore();
    }

    private void UpdateTotalScore()
    {
        totalScoreText.text = totalScore.ToString();
        RectMaxWidth = totalScoreBarFillMax.rect.width;

        var RectCurrentWidth = totalScoreBarFill.rect.width;
        var pastScoreThresholds = ScoreThresholds.GetCurrentThreshold(ScoreThresholds.GetTier(totalScore) - 1);

        var divisor = totalScore - pastScoreThresholds;
        if (divisor <= 0)
        {
            divisor = 1;
        }

        var dividend = (ScoreThresholds.GetCurrentThreshold(ScoreThresholds.GetTier(totalScore)) - pastScoreThresholds);
        
        float fill_percentage = divisor / dividend;
        if (fill_percentage >= 1.0f)
        {
            fill_percentage = 0.0f;
        }
        totalScoreBarFill.sizeDelta = new Vector2(RectMaxWidth * fill_percentage, 100);
    }

    // Increase increment of score by 1 unit until it has reached the final sum. 
    // Will also increase the threshold as necessary.
    private void IncrementTotalScore()
    {
        if (totalScore >= finalTotalScore)
        {
            IsUpdating = false;
            return;
        }
        if (totalScore >= ScoreThresholds.GetCurrentThreshold(ScoreThresholds.CurrentTier))
        {
            ScoreThresholds.NextTier();

            // Increment highest tier if there is one larger, and display character unlock pane.
            var gamedata_handler = gameData.GetComponent<GamedataHandler>();
            gamedata_handler.Load();

            Debug.Log("Current Tier: " + ScoreThresholds.CurrentTier);
            Debug.Log("Highest Tier: " + gamedata_handler.Data.HighestTier);

            if (ScoreThresholds.CurrentTier > gamedata_handler.Data.HighestTier)
            {
                gamedata_handler.Data.HighestTier = ScoreThresholds.CurrentTier;
                gamedata_handler.Save();
                IsUpdating = false;
                var char_unlock = Instantiate(CharacterUnlockPrefab, Canvas);
                char_unlock.GetComponent<UnlockCharHandler>().Setup(ScoreThresholds.CurrentTier - 2, this);
            }
        }
        totalScore += scoreIncrement;
        scoreIncrement += 1;
    }

    public void SubmitScore()
    {
        isWaiting = false;
    }

    public string GetSubmit()
    {
        if (isWaiting == false)
        {
            isWaiting = true;
            return inputName.text;
        }
        else
        {
            return null;
        }
    }

    public void SwitchLeaderBoardPanel()
    {
        //animate game over score panel out of the way
        
    }
	
	public void ReturnToCandidateSelect()
    {
        gameData.GetComponent<GamedataHandler>().Destroy();

        SceneManager.LoadSceneAsync(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
