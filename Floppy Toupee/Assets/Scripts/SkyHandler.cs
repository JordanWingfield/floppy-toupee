﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyHandler : MonoBehaviour {

    public float Speed = 1f;
    public bool IsDay = true;
    public float AngleCounter = 0f;
    public float TimerCounter = 1f;
    public Transform SkyTransform;
    public SpriteRenderer Sky;
    public SpriteRenderer Sun;
    public SpriteRenderer Moon;

	void Start () {
        Sky = GetComponent<SpriteRenderer>();
	}
	
	void Update () {
        UpdateRotation();
        UpdateSkyColour();
        ToggleSunMoon();
	}
    
    private void UpdateRotation()
    {
        SkyTransform.Rotate(0, 0, -Time.deltaTime * Speed);
    }

    private void UpdateSkyColour()
    {
        var r = Sky.color.r;
        var g = Sky.color.g;
        var b = Sky.color.b;
        var a = Sky.color.a;

        Debug.Log("A: " + AngleCounter / 255);

        Sky.color = new Color(r, g, b, (AngleCounter / 255));

    }

    private void ToggleSunMoon()
    {
        if (IsDay)
        {
            AngleCounter -= Time.deltaTime * Speed;
            TimerCounter -= Time.deltaTime * Speed;
        }
        else if (!IsDay)
        {
            AngleCounter += Time.deltaTime * Speed;
            TimerCounter += Time.deltaTime * Speed;
        }

        if (TimerCounter > 90 || TimerCounter <= -90)
        {
            IsDay = !IsDay;

            if (IsDay)
            {
                Sun.enabled = true;
                Moon.enabled = false;
            }
            else
            {
                Sun.enabled = false;
                Moon.enabled = true;
            }
        }

    }
}
